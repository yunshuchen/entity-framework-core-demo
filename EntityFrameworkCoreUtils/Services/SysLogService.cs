﻿using EntityFrameworkCoreUtils.Models;
using EntityFrameworkCoreUtils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkCoreUtils.Services
{
    [ScopedDI]
    public class SysLogService :  ServiceBase<sys_log, Microsoft.Extensions.Logging.ILogger<sys_log>>, ISysLogService
    {

        public SysLogService(IRepository<sys_log> _useService, Microsoft.Extensions.Logging.ILogger<sys_log> _log)
            : base(_useService, _log)
        {
  
        }
    }
}
