﻿using EntityFrameworkCoreUtils.Models;
using EntityFrameworkCoreUtils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkCoreUtils.Services
{
    public interface ISysLogService:IService<sys_log>
    {
    }
}
