using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EntityFrameworkCoreUtils.Models
{
    [Serializable]
    [Table("sys_resource")]
    public class sys_resource
    {

        public long id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public long p_module { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string res_code { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string res_name { get; set; }

        /// <summary>        
        ///
        /// </summary>
        public string uri { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string url { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public int n_sort { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string description { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public int delete_status { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public int enable_status { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public int status { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string last_modifier_id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string last_modifier_name { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public DateTime last_modified_time { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string creator_id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string creator_name { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public DateTime created_time { get; set; }
    }
}
