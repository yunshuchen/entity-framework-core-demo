﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EntityFrameworkCoreUtils.Models
{
    [Table("sys_logs")]
    public class sys_log
    {

        /// <summary>
        /// 主键自增长
        /// </summary>
        [Key]
        //[ScaffoldColumn(false)]
        //非自增长
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        //自增长
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 日志模块
        /// </summary>
        public string log_module { get; set; }
        /// <summary>
        /// 模块名称
        /// </summary>
        public string log_module_name { get; set; }
        /// <summary>
        /// 操作用户ID
        /// </summary>
        public long log_user { get; set; }
        /// <summary>
        /// 操作用户名称
        /// </summary>
        public string log_user_name { get; set; }
        /// <summary>
        /// 操作类型
        /// </summary>
        public int operate_type { get; set; }

        /// <summary>
        /// 操作类型名称
        /// </summary>
        public string operate_type_name { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// IP
        /// </summary>
        public string log_ip { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime log_time { get; set; }

        [NotMapped]
        public string token { get; set; }
    }
}
