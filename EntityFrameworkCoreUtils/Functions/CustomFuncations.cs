﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkCoreUtils.Functions
{
   public class CustomFuncations
    {
        [DbFunction(Name = "GetDistance", Schema = "dbo")]
        public static int GetDistance(double lngBegin, double latBegin, double lngEnd, double latEnd)
        {
            throw new NotSupportedException("Direct calls are not supported.");
        }
    }
}
