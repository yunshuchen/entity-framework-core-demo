﻿using EntityFrameworkCoreUtils.Models;
using EntityFrameworkCoreUtils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkCoreUtils.Mapping
{
    public class sys_resource_mapping : NopEntityTypeConfiguration<sys_resource>
    {

        public override void Configure(EntityTypeBuilder<sys_resource> builder)
        {
            builder.ToTable(nameof(sys_resource));
            //builder.ToTable("sys_logs");
            builder.HasKey(sysFun => sysFun.id);
 

            base.Configure(builder);
        }
    }
}
