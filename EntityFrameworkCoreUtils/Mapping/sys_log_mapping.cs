﻿using EntityFrameworkCoreUtils.Models;
using EntityFrameworkCoreUtils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace EntityFrameworkCoreUtils.Mapping
{
    public class sys_log_mapping : NopEntityTypeConfiguration<sys_log>
    {

        public override void Configure(EntityTypeBuilder<sys_log> builder)
        {
            //builder.ToTable(nameof(sys_log));
            builder.ToTable("sys_logs");
            builder.HasKey(sysFun => sysFun.id);
            

            base.Configure(builder);
        }
    }
}
