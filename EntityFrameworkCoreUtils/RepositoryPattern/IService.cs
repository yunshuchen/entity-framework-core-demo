using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace EntityFrameworkCoreUtils.RepositoryPattern
{
    public interface IService<T> : IDisposable
        where T : class
    {


        T GetByOne(string OrderBy, Expression<Func<T, bool>> predicate);
        T GetByOne(Expression<Func<T, bool>> predicate);
        int Count(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// 获取数量
        /// </summary>
        /// <param name="ExpressionsSql">id=@0</param>
        /// <param name="ExpressionPartams">123</param>
        /// <returns></returns>
        int Count(string ExpressionsSql, params object[] ExpressionPartams);

        /// <summary>
        /// 获取数量
        /// </summary>
        /// <param name="ExpressionsSql">查询条件</param>
        /// <param name="ExpressionPartams">查询条件</param>
        /// <returns></returns>
        int Count(string ExpressionsSql);

        List<T> GetList(string keySelector, List<Expression<Func<T, bool>>> predicate);
        /// <summary>
        /// 查询列表根据Sql条件
        /// </summary>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql">id=@0</param>
        /// <param name="ExpressionPartams">125</param>
        /// <returns></returns>
        List<T> GetList(string OrderBy, string ExpressionsSql, params object[] ExpressionPartams);
        /// <summary>
        /// 查询列表根据Sql条件
        /// </summary>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql">id=@0</param>
        /// <returns></returns>
        List<T> GetList(string OrderBy, string ExpressionsSql);


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="OrderBy"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, List<Expression<Func<T, bool>>> predicate);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql"></param>
        /// <returns></returns>
        PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, string ExpressionsSql);
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql"></param>
        /// <param name="ExpressionPartams"></param>
        /// <returns></returns>
        PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, string ExpressionsSql, params object[] ExpressionPartams);


        /// <summary>
        /// 修改实体
        /// 修改依据为主键
        /// 修改项为主键之外的所有字段
        /// </summary>
        /// <param name="content"></param>
        void Modify(T content);
        void Modify(IEnumerable<T> entities);
        int Modify(Expression<Func<T, bool>> condition, Expression<Func<T, T>> content);


        int Delete(Expression<Func<T, bool>> condition);

        int Delete(T model);
        int Delete(IEnumerable<T> entities);

        T Insert(T item);
        void BulkInsert(IEnumerable<T> model);



    }
}