
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EntityFrameworkCoreUtils.Models;
using Microsoft.EntityFrameworkCore;

namespace EntityFrameworkCoreUtils.RepositoryPattern
{

    public abstract class ServiceBase<T, LogT> : IService<T>
        where T : class
        where LogT : Microsoft.Extensions.Logging.ILogger<T>
    {
        #region ����
        private readonly Microsoft.Extensions.Logging.ILogger<T> log;

        private readonly IRepository<T> useService;
        #endregion

        public ServiceBase(IRepository<T> _useService, Microsoft.Extensions.Logging.ILogger<T> _log)
        {
            log = _log;
            useService = _useService;
        }

        public void BulkInsert(IEnumerable<T> model)
        {
            useService.BulkInsert(model);
        }

        public int Count(Expression<Func<T, bool>> predicate)
        {
            return useService.Count(predicate);
        }

        public int Count(string ExpressionsSql, params object[] ExpressionPartams)
        {
            return useService.Count(ExpressionsSql, ExpressionPartams);
        }

        public int Count(string ExpressionsSql)
        {
            return useService.Count(ExpressionsSql);
        }
        public int Delete(T model)
        {
            useService.Delete(model);
            return 1;
        }


        public int Delete(IEnumerable<T> entities)
        {
            useService.Delete(entities);
            return 1;
        }

        public int Delete(Expression<Func<T, bool>> condition)
        {
            return useService.Delete(condition);
        }

        public void Dispose()
        {

        }

        public T GetByOne(string OrderBy, Expression<Func<T, bool>> predicate)
        {
            return useService.GetOne(OrderBy, predicate);
        }

        public T GetByOne(Expression<Func<T, bool>> predicate)
        {
            return useService.GetOne("", predicate);
        }

        public List<T> GetList(string OrderBy, List<Expression<Func<T, bool>>> predicate)
        {
            return useService.GetList(OrderBy, predicate);
        }

        public List<T> GetList(string OrderBy, string ExpressionsSql, params object[] ExpressionPartams)
        {
            return useService.GetList(OrderBy, ExpressionsSql, ExpressionPartams);
        }

        public List<T> GetList(string OrderBy, string ExpressionsSql)
        {
            return useService.GetList(OrderBy, ExpressionsSql);
        }

        public PageOf<T> GetPageList(int iPage, int PageSize, string keySelector, List<Expression<Func<T, bool>>> predicate)
        {
            return useService.GetPageList(iPage, PageSize, keySelector, predicate);
        }

        public PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, string ExpressionsSql)
        {
            return useService.GetPageList(iPage, PageSize, OrderBy, ExpressionsSql);
        }

        public PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, string ExpressionsSql, params object[] ExpressionPartams)
        {
            return useService.GetPageList(iPage, PageSize, OrderBy, ExpressionsSql, ExpressionPartams);
        }

        public T Insert(T item)
        {
            return useService.Insert(item);
        }

        public int Modify(Expression<Func<T, bool>> condition, Expression<Func<T, T>> content)
        {
            return useService.Update(condition, content);
        }

        public void Modify(T content)
        {
            useService.Update(content);
        }

        public void Modify(IEnumerable<T> entities)
        {
            useService.Update(entities);
        }

        public void Test()
        {

            //log LEFT JOIN res
            //var joinResults = useService.OtherTable<sys_log>()
            //                .GroupJoin(useService.OtherTable<sys_resource>(), log => log.log_module, res => res.res_code, (log, res) => new { log, res })
            //                .SelectMany(combin => combin.res.DefaultIfEmpty(), (log, res) => new
            //                {
            //                    log.log.id,
            //                    log.log.log_module,
            //                    res.res_name,
            //                    diff_ddat= Microsoft.EntityFrameworkCore.EF.Functions.DateDiffDay(DateTime.Now,log.log.log_time)
            //                }).ToList();

            

            //return useService.OtherTable<sys_log>().Join(useService.OtherTable<sys_resource>(), log => log.log_module, res => res.res_code, (log, res) => new
            //{
            //    log.id,
            //    log.log_module,
            //    res.res_name
            //}).ToList();

            //var result = from l in useService.OtherTable<sys_log>()
            //             join r in useService.OtherTable<sys_resource>()
            //             on l.log_module equals r.res_code
            //             select new
            //             {
            //                 l.id,
            //                 l.log_module,
            //                 r.res_name
            //             };

            //result.ToList();
        }
    }
}