﻿using EntityFrameworkCoreUtils.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using TestWeb.Models;

namespace TestWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index([FromServices] ISysLogService logService)
        {
            //var model=logService.GetByOne(a => a.id == 1);
            // Console.WriteLine(logService.Count(a => a.operate_type == 3));
            // Console.WriteLine(logService.Count("operate_type=3"));
            //  model.log_module_name = "修改";
            //logService.Modify(a => a.id == 2, a => new EntityFrameworkCoreUtils.Models.sys_log() { log_module_name = "1223" });

            var logs_list=logService.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<EntityFrameworkCoreUtils.Models.sys_log, bool>>>() {
                a=>a.id<=10
            });
            //logs_list.ForEach(a => a.log_module_name = "update");
            //logService.Modify(logs_list);
            //logService.Delete(logs_list)
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
