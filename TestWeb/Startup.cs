using Autofac;
using EntityFrameworkCoreUtils;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Routing;

namespace TestWeb
{
    public class Startup
    {

        #region 属性
        /// <summary>
        /// 服务
        /// </summary>
        private IServiceProvider _serviceProvider { get; set; }
        /// <summary>
        /// 服务
        /// </summary>
        public virtual IServiceProvider ServiceProvider => _serviceProvider;
        #endregion

        #region Utilities

        /// <summary>
        /// Get IServiceProvider
        /// </summary>
        /// <returns>IServiceProvider</returns>
        protected IServiceProvider GetServiceProvider()
        {
            var accessor = ServiceProvider.GetService<IHttpContextAccessor>();
            var context = accessor.HttpContext;
            //var a=context == null ? null : RequestServices ?? ServiceProvider;  context==null?null:SericeProvider

            return context?.RequestServices ?? ServiceProvider;
        }
        /// <summary>
        /// Autofac 自动注入
        /// </summary>
        /// <param name="builder"></param>
        public void ConfigureContainer(ContainerBuilder builder)
        {

            //数据库
            builder.Register(context => new NopObjectContext(context.Resolve<DbContextOptions<NopObjectContext>>()))
                .As<IDbContext>().InstancePerLifetimeScope();

            //IOC
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();


            var _assably = AppDomain.CurrentDomain.GetAssemblies();
            var _needImpl = _assably.Where(c => c.FullName.StartsWith("EntityFrameworkCoreDemo") || c.FullName.StartsWith("EntityFrameworkCoreUtils")).ToArray();

            builder.RegisterAssemblyTypes(_needImpl)
                         .Where(t => t.GetCustomAttribute<ScopedDIAttribute>() != null)
                         .AsImplementedInterfaces()
                        .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(_needImpl)
                       .Where(t => t.GetCustomAttribute<TransientDIAttribute>() != null)
                       .AsImplementedInterfaces()
                      .InstancePerDependency();

            builder.RegisterAssemblyTypes(_needImpl)
                        .Where(t => t.GetCustomAttribute<SingletonDIAttribute>() != null)
                        .AsImplementedInterfaces()
                       .SingleInstance();

            // var list = _needImpl[1].GetTypes().Where(item => Attribute.IsDefined(item, typeof(SingletonDIAttribute)));


            #region 注入没有接口的类


            var list = _needImpl.Select(a => a.GetTypes().Where(item => Attribute.IsDefined(item, typeof(ClassTypeDIAttribute)))).ToList();

            foreach (var item in list)
            {
                foreach (var job in item)
                {
                    if (Attribute.IsDefined(job, typeof(SingletonTypeDIAttribute)))
                    {
                        builder.RegisterType(job)
                                  .SingleInstance();
                    }
                    else if (Attribute.IsDefined(job, typeof(TransientTypeDIAttribute)))
                    {
                        builder.RegisterType(job)
                                  .InstancePerDependency();
                    }
                    else if (Attribute.IsDefined(job, typeof(ScopedTypeDIAttribute)))
                    {
                        builder.RegisterType(job)
                                  .InstancePerLifetimeScope();
                    }
                }
            }

            builder.RegisterBuildCallback(lifetimeScope =>
            {

                Console.WriteLine("全部注入成功");
                // ServiceProviderInstance.Container = lifetimeScope as IContainer;
            });
            #endregion


            #region 方法1   Load 适用于无接口注入
            //var assemblysServices = Assembly.Load("Exercise.Services");

            //containerBuilder.RegisterAssemblyTypes(assemblysServices)
            //          .AsImplementedInterfaces()
            //          .InstancePerLifetimeScope();

            //var assemblysRepository = Assembly.Load("Exercise.Repository");

            //containerBuilder.RegisterAssemblyTypes(assemblysRepository)
            //          .AsImplementedInterfaces()
            //          .InstancePerLifetimeScope();

            #endregion

            #region 方法2  选择性注入 与方法1 一样
            //            Assembly Repository = Assembly.Load("Exercise.Repository");
            //            Assembly IRepository = Assembly.Load("Exercise.IRepository");
            //            containerBuilder.RegisterAssemblyTypes(Repository, IRepository)
            //.Where(t => t.Name.EndsWith("Repository"))
            //.AsImplementedInterfaces().PropertiesAutowired();

            //            Assembly service = Assembly.Load("Exercise.Services");
            //            Assembly Iservice = Assembly.Load("Exercise.IServices");
            //            containerBuilder.RegisterAssemblyTypes(service, Iservice)
            //.Where(t => t.Name.EndsWith("Service"))
            //.AsImplementedInterfaces().PropertiesAutowired();
            #endregion

            #region 方法3  使用 LoadFile 加载服务层的程序集  将程序集生成到bin目录 实现解耦 不需要引用
            //获取项目路径
            //var basePath = Microsoft.DotNet.PlatformAbstractions.ApplicationEnvironment.ApplicationBasePath;
            //var ServicesDllFile = Path.Combine(basePath, "Exercise.Services.dll");//获取注入项目绝对路径
            //var assemblysServices = Assembly.LoadFile(ServicesDllFile);//直接采用加载文件的方法
            //containerBuilder.RegisterAssemblyTypes(assemblysServices).AsImplementedInterfaces();

            //var RepositoryDllFile = Path.Combine(basePath, "Exercise.Repository.dll");
            //var RepositoryServices = Assembly.LoadFile(RepositoryDllFile);//直接采用加载文件的方法
            //containerBuilder.RegisterAssemblyTypes(RepositoryServices).AsImplementedInterfaces();

            //获取方式
            //[FromServices] Autofac.IComponentContext componentContext
            //    var service = componentContext.Resolve<ISiteSurveyAnswerService>();
            #endregion
        }

       
        #endregion
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        string mysql_connection = "Server=192.168.168.137;Database=skycms_gm5297;User Id=dev;Password=dev123321;CharSet=utf8mb4;";
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<NopObjectContext>(options => options.UseLazyLoadingProxies().UseMySql(connectionString: mysql_connection, serverVersion:ServerVersion.AutoDetect(mysql_connection)));
            services.AddEntityFrameworkMySql();
            services.AddEntityFrameworkProxies();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(GetRoutes);
        }

        private readonly Action<IEndpointRouteBuilder> GetRoutes =
            endpoints =>
            {

                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapAreaControllerRoute(
                  name: "areas", "areas",
                  pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");


                //endpoints.MapRazorPages();
            };

    }
}
